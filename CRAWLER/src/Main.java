

public class Main 
{
	private static String addres="students.txt";
	public static void main( String[] args ) throws Exception
	{
		final Logger[] loggers = new Logger[]
		{
			new ConsoleLogger(),
			new MailLogger()
		};
		crawler cra=new crawler(addres);
		cra.addIterationStartedListener((iteration)->System.out.println("Rozpoczeto iteracje "+iteration));
		cra.addIterationComplitedListener(iteration->System.out.println("Zakonczono iteracje\n"));
		cra.addStudentaddedListener((Student)->loggers[0].log("ADDED",Student));
		cra.addStudentaddedListener((Student)->loggers[1].log("ADDED",Student));
		cra.addStudentremovedListener((Student)->loggers[0].log("REMOVED",Student));
		cra.addStudentremovedListener((Student)->loggers[1].log("REMOVED",Student));
		try {
			cra.run();
		}catch (crawlerException e){
			e.printStackTrace();
			e.toString();
		}
			catch(Exception e) {

			e.printStackTrace();
			System.out.println("ERROR");
		}
	}
}