
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MailLogger implements Logger {

    @Override
    public void log(String status, Student student) {
        String message_text=status+": "+ student.getMark() + " "+ student.getFirstName()+ " "+ student.getLastName()+ " "+ student.getAge();

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("java.test.crawler@gmail.com","crawlertest");
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("java.test.crawler@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("java.test.crawler@gmail.com"));
            message.setSubject("Crawler Update");
            message.setText( message_text);

            Transport.send(message);



        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }
}