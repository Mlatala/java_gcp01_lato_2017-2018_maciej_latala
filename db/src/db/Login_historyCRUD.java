package db;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Login_historyCRUD {
	 
	 static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	 static final String DB_URL = "jdbc:mysql://192.168.0.115:3306/javadb?autoReconnect=true&useSSL=false";

	 
	 static final String USER = "root";
	 static final String PASS = "root";
	 
	 public static void Create(int user_id){
		 Connection conn = null;
		 Statement stmt = null;
		 try{
		    Class.forName("com.mysql.jdbc.Driver");

		    System.out.println("Connecting to database...");
		    conn = DriverManager.getConnection(DB_URL,USER,PASS);

		    stmt = conn.createStatement();
		    String sql;
		    sql = "INSERT INTO login_history VALUES "+user_id+", NOW()";
		    stmt.executeQuery(sql);
		    
		    stmt.close();
		    conn.close();
		 }
		 catch(SQLException se){
			    se.printStackTrace();
			 }catch(Exception e){
			    e.printStackTrace();
			 }finally{
			    try{
			       if(stmt!=null)
			          stmt.close();
			    }catch(SQLException se2){
			    }
			    try{
			       if(conn!=null)
			          conn.close();
			    }catch(SQLException se){
			       se.printStackTrace();
			    }
			 }
		
	};
	public static void Read() {
		 Connection conn = null;
		 Statement stmt = null;
		 try{
		    Class.forName("com.mysql.jdbc.Driver");

		    System.out.println("Connecting to database...");
		    conn = DriverManager.getConnection(DB_URL,USER,PASS);

		    stmt = conn.createStatement();
		    String sql;
		    sql = "SELECT * FROM login_history";
		    ResultSet rs = stmt.executeQuery(sql);

		    while(rs.next()){
		       int id  = rs.getInt("user_id");
		       Date date = rs.getDate("date");

		       System.out.print("USER_ID: " + id);
		       System.out.println(", Date: " + date);
		    }
		    rs.close();
		    stmt.close();
		    conn.close();
		 }catch(SQLException se){
		    se.printStackTrace();
		 }catch(Exception e){
		    e.printStackTrace();
		 }finally{
		    try{
		       if(stmt!=null)
		          stmt.close();
		    }catch(SQLException se2){
		    }
		    try{
		       if(conn!=null)
		          conn.close();
		    }catch(SQLException se){
		       se.printStackTrace();
		    }
		 }
		};
	 public static void Update(int user_id,Date date){
		 Connection conn = null;
		 Statement stmt = null;
		 try{
		    Class.forName("com.mysql.jdbc.Driver");

		    System.out.println("Connecting to database...");
		    conn = DriverManager.getConnection(DB_URL,USER,PASS);

		    stmt = conn.createStatement();
		    String sql;
		    sql = "UPDATE login_history SET user_id='"+user_id+"',date='"+date+"'";
		    stmt.executeQuery(sql);
		    
		    stmt.close();
		    conn.close();
		 }
		 catch(SQLException se){
			    se.printStackTrace();
			 }catch(Exception e){
			    e.printStackTrace();
			 }finally{
			    try{
			       if(stmt!=null)
			          stmt.close();
			    }catch(SQLException se2){
			    }
			    try{
			       if(conn!=null)
			          conn.close();
			    }catch(SQLException se){
			       se.printStackTrace();
			    }
			 }
	 	};
	 	public static void Delete(int user_id){
			 Connection conn = null;
			 Statement stmt = null;
			 try{
			    Class.forName("com.mysql.jdbc.Driver");

			    System.out.println("Connecting to database...");
			    conn = DriverManager.getConnection(DB_URL,USER,PASS);

			    stmt = conn.createStatement();
			    String sql;
			    sql = "DELETE FROM login_history WHERE user_id="+user_id;
			    stmt.executeQuery(sql);
			    
			    stmt.close();
			    conn.close();
			 }
			 catch(SQLException se){
				    se.printStackTrace();
				 }catch(Exception e){
				    e.printStackTrace();
				 }finally{
				    try{
				       if(stmt!=null)
				          stmt.close();
				    }catch(SQLException se2){
				    }
				    try{
				       if(conn!=null)
				          conn.close();
				    }catch(SQLException se){
				       se.printStackTrace();
				    }
				 }
		 	};
	 	
}
