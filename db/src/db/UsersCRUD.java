package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UsersCRUD {
	 
	 static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	 static final String DB_URL = "jdbc:mysql://192.168.0.115:3306/javadb?autoReconnect=true&useSSL=false";

	 
	 static final String USER = "root";
	 static final String PASS = "root";
	 
	 public static void Create(String username,String password, int age){
		 Connection conn = null;
		 Statement stmt = null;
		 try{
		    Class.forName("com.mysql.jdbc.Driver");

		    System.out.println("Connecting to database...");
		    conn = DriverManager.getConnection(DB_URL,USER,PASS);

		    stmt = conn.createStatement();
		    String sql;
		    sql = "INSERT INTO users VALUES NULL,'"+username+"','"+password+"','"+age+"'";
		    stmt.executeQuery(sql);
		    
		    stmt.close();
		    conn.close();
		 }
		 catch(SQLException se){
			    se.printStackTrace();
			 }catch(Exception e){
			    e.printStackTrace();
			 }finally{
			    try{
			       if(stmt!=null)
			          stmt.close();
			    }catch(SQLException se2){
			    }
			    try{
			       if(conn!=null)
			          conn.close();
			    }catch(SQLException se){
			       se.printStackTrace();
			    }
			 }
		
	};
	public static void Read() {
		 Connection conn = null;
		 Statement stmt = null;
		 try{
		    Class.forName("com.mysql.jdbc.Driver");

		    System.out.println("Connecting to database...");
		    conn = DriverManager.getConnection(DB_URL,USER,PASS);

		    stmt = conn.createStatement();
		    String sql;
		    sql = "SELECT user_id, username,password,age FROM users";
		    ResultSet rs = stmt.executeQuery(sql);

		    while(rs.next()){
		       int id  = rs.getInt("user_id");
		       int age = rs.getInt("age");
		       String username = rs.getString("username");
		       String password = rs.getString("password");

		       System.out.print("ID: " + id);
		       System.out.print(", Age: " + age);
		       System.out.print(", Usename: " + username);
		       System.out.println(", Password: " + password);
		    }
		    rs.close();
		    stmt.close();
		    conn.close();
		 }catch(SQLException se){
		    se.printStackTrace();
		 }catch(Exception e){
		    e.printStackTrace();
		 }finally{
		    try{
		       if(stmt!=null)
		          stmt.close();
		    }catch(SQLException se2){
		    }
		    try{
		       if(conn!=null)
		          conn.close();
		    }catch(SQLException se){
		       se.printStackTrace();
		    }
		 }
		};
	 public static void Update(String username,String password, int age){
		 Connection conn = null;
		 Statement stmt = null;
		 try{
		    Class.forName("com.mysql.jdbc.Driver");

		    System.out.println("Connecting to database...");
		    conn = DriverManager.getConnection(DB_URL,USER,PASS);

		    stmt = conn.createStatement();
		    String sql;
		    sql = "UPDATE users SET username='"+username+"',password='"+password+"'age='"+age+"'";
		    stmt.executeQuery(sql);
		    
		    stmt.close();
		    conn.close();
		 }
		 catch(SQLException se){
			    se.printStackTrace();
			 }catch(Exception e){
			    e.printStackTrace();
			 }finally{
			    try{
			       if(stmt!=null)
			          stmt.close();
			    }catch(SQLException se2){
			    }
			    try{
			       if(conn!=null)
			          conn.close();
			    }catch(SQLException se){
			       se.printStackTrace();
			    }
			 }
	 	};
	 	public static void Delete(int user_id){
			 Connection conn = null;
			 Statement stmt = null;
			 try{
			    Class.forName("com.mysql.jdbc.Driver");

			    System.out.println("Connecting to database...");
			    conn = DriverManager.getConnection(DB_URL,USER,PASS);

			    stmt = conn.createStatement();
			    String sql;
			    sql = "DELETE FROM users WHERE user_id="+user_id;
			    stmt.executeQuery(sql);
			    
			    stmt.close();
			    conn.close();
			 }
			 catch(SQLException se){
				    se.printStackTrace();
				 }catch(Exception e){
				    e.printStackTrace();
				 }finally{
				    try{
				       if(stmt!=null)
				          stmt.close();
				    }catch(SQLException se2){
				    }
				    try{
				       if(conn!=null)
				          conn.close();
				    }catch(SQLException se){
				       se.printStackTrace();
				    }
				 }
		 	};
	 	
}
