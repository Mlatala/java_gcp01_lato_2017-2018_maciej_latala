package db;

import java.sql.*;

public class Login {

 static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
 static final String DB_URL = "jdbc:mysql://192.168.0.115:3306/javadb?autoReconnect=true&useSSL=false";

 static final String USER = "root";
 static final String PASS = "root";
 
 public static void login() {
	 Connection conn = null;
	 Statement stmt = null;
	 try{
	    
	    Class.forName("com.mysql.jdbc.Driver");

	    System.out.println("Connecting to database...");
	    conn = DriverManager.getConnection(DB_URL,USER,PASS);
	    
	    stmt = conn.createStatement();
	    String sql;
	    sql = "SELECT * FROM userview";
	    ResultSet rs = stmt.executeQuery(sql);

	    
	    while(rs.next()){
	       
	       String username = rs.getString("username");
	       String password = rs.getString("password");
	       if (Main.username_tf.getText()==username && Main.password_tf.getText()==password)
	       {
	       System.out.print("you logged in");
	       break;
	       }
	       else{
	       System.out.println("invalid username or password");
	       break;
	       }
	    }
	    rs.close();
	    stmt.close();
	    conn.close();
	 }catch(SQLException se){
	    se.printStackTrace();
	 }catch(Exception e){
	    e.printStackTrace();
	 }finally{
	    try{
	       if(stmt!=null)
	          stmt.close();
	    }catch(SQLException se2){
	    }
	    try{
	       if(conn!=null)
	          conn.close();
	    }catch(SQLException se){
	       se.printStackTrace();
	    }
	 }
	}
 
}