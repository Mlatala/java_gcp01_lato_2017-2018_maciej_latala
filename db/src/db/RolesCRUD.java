package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RolesCRUD {
	 
	 static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	 static final String DB_URL = "jdbc:mysql://192.168.0.115:3306/javadb?autoReconnect=true&useSSL=false";

	 
	 static final String USER = "root";
	 static final String PASS = "root";
	 
	 public static void Create(int user_id,String role){
		 Connection conn = null;
		 Statement stmt = null;
		 try{
		    Class.forName("com.mysql.jdbc.Driver");

		    System.out.println("Connecting to database...");
		    conn = DriverManager.getConnection(DB_URL,USER,PASS);

		    stmt = conn.createStatement();
		    String sql;
		    sql = "INSERT INTO roles VALUES "+user_id+",'"+role+"'";
		    stmt.executeQuery(sql);
		    
		    stmt.close();
		    conn.close();
		 }
		 catch(SQLException se){
			    se.printStackTrace();
			 }catch(Exception e){
			    e.printStackTrace();
			 }finally{
			    try{
			       if(stmt!=null)
			          stmt.close();
			    }catch(SQLException se2){
			    }
			    try{
			       if(conn!=null)
			          conn.close();
			    }catch(SQLException se){
			       se.printStackTrace();
			    }
			 }
		
	};
	public static void Read() {
		 Connection conn = null;
		 Statement stmt = null;
		 try{
		    Class.forName("com.mysql.jdbc.Driver");

		    System.out.println("Connecting to database...");
		    conn = DriverManager.getConnection(DB_URL,USER,PASS);

		    stmt = conn.createStatement();
		    String sql;
		    sql = "SELECT * FROM roles";
		    ResultSet rs = stmt.executeQuery(sql);

		    while(rs.next()){
		       int id  = rs.getInt("user_id");
		       String role = rs.getString("role");

		       System.out.print("USER_ID: " + id);
		       System.out.println(", Role: " + role);
		    }
		    rs.close();
		    stmt.close();
		    conn.close();
		 }catch(SQLException se){
		    se.printStackTrace();
		 }catch(Exception e){
		    e.printStackTrace();
		 }finally{
		    try{
		       if(stmt!=null)
		          stmt.close();
		    }catch(SQLException se2){
		    }
		    try{
		       if(conn!=null)
		          conn.close();
		    }catch(SQLException se){
		       se.printStackTrace();
		    }
		 }
		};
	 public static void Update(int user_id,String role){
		 Connection conn = null;
		 Statement stmt = null;
		 try{
		    Class.forName("com.mysql.jdbc.Driver");

		    System.out.println("Connecting to database...");
		    conn = DriverManager.getConnection(DB_URL,USER,PASS);

		    stmt = conn.createStatement();
		    String sql;
		    sql = "UPDATE roles SET user_id='"+user_id+"',role='"+role+"'";
		    stmt.executeQuery(sql);
		 
		    stmt.close();
		    conn.close();		    
		 }
		 catch(SQLException se){
			    se.printStackTrace();
			 }catch(Exception e){
			    e.printStackTrace();
			 }finally{
			    try{
			       if(stmt!=null)
			          stmt.close();
			    }catch(SQLException se2){
			    }
			    try{
			       if(conn!=null)
			          conn.close();
			    }catch(SQLException se){
			       se.printStackTrace();
			    }
			 }
	 	};
	 	public static void Delete(int user_id){
			 Connection conn = null;
			 Statement stmt = null;
			 try{
			    Class.forName("com.mysql.jdbc.Driver");

			    System.out.println("Connecting to database...");
			    conn = DriverManager.getConnection(DB_URL,USER,PASS);

			    stmt = conn.createStatement();
			    String sql;
			    sql = "DELETE FROM roles WHERE user_id="+user_id;
			    stmt.executeQuery(sql);
			    
			    stmt.close();
			    conn.close();
			 }
			 catch(SQLException se){
				    se.printStackTrace();
				 }catch(Exception e){
				    e.printStackTrace();
				 }finally{
				    try{
				       if(stmt!=null)
				          stmt.close();
				    }catch(SQLException se2){
				    }
				    try{
				       if(conn!=null)
				          conn.close();
				    }catch(SQLException se){
				       se.printStackTrace();
				    }
				 }
		 	};
	 	
}
