package db;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application{
		Button login;
		Button view_user_table;
		Button view_login_hitory_table;
		Button view_roles_table;
		
		static TextField username_tf;
		static TextField password_tf;
		
	public static void main(String[] args){
		launch(args);
		}
	@Override
	public void start(Stage primaryStage)throws Exception{

		
		login=new Button();
		view_login_hitory_table=new Button();
		view_roles_table=new Button();
		view_user_table=new Button();
		
		username_tf=new TextField();
		username_tf.setPromptText("username");
		password_tf=new TextField();
		password_tf.setPromptText("password");
		
		login.setText("login");
		view_login_hitory_table.setText("view_login_hitory_table");
		view_roles_table.setText("view_roles_table");
		view_user_table.setText("view_user_table");
		
		login.setOnAction(e -> Login.login());
		view_user_table.setOnAction(e -> UsersCRUD.Read());
		view_roles_table.setOnAction(e -> RolesCRUD.Read());
		view_login_hitory_table.setOnAction(e ->Login_historyCRUD.Read());
		
		VBox layout = new VBox(10);
		layout.getChildren().addAll(username_tf,password_tf,login,view_login_hitory_table,view_roles_table,view_user_table);
		
		Scene scene= new Scene(layout,200,300);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
