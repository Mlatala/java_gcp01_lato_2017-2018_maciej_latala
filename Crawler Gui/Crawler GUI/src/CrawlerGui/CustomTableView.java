package CrawlerGui;

import javafx.scene.layout.AnchorPane;

public class CustomTableView extends AnchorPane{
	
	private double mark;
	private String firstName;
	private String lastName;
	private int age;
	
	public CustomTableView(){
		this.mark=2.0;
		this.firstName="";
		this.lastName="";
		this.age=0;
	}
	
	public CustomTableView(double mark,String firstName,String lastName, int age){
		this.mark=mark;
		this.firstName=firstName;
		this.lastName=lastName;
		this.age=age;
	}
	public double getMark()
	{
		return this.mark;
	}
	
	public void setMark( double mark )
	{
		this.mark = mark;
	}
	
	public String getFirstName()
	{
		return this.firstName;
	}
	
	public void setFirstName( String firstName )
	{
		this.firstName = firstName;
	}
	
	public String getLastName()
	{
		return this.lastName;
	}
	
	public void setLastName( String lastName )
	{
		this.lastName = lastName;
	}
	
	public int getAge()
	{
		return this.age;
	}
	
	public void setAge( int age )
	{
		this.age = age;
	}
}
 