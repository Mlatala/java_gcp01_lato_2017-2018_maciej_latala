package CrawlerGui;

@FunctionalInterface
public interface StudentListener {
	void handle(Student student);
}
