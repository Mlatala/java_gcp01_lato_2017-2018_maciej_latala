package CrawlerGui;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.*;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.event.*;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.*;
import javafx.stage.Stage;
import sun.font.CreatedFontTracker;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.paint.Color;


public class Main extends Application{
	
	 TableView<CustomTableView> studentsTab;
	 TableView<LogItem> logsTab;
	
	public static void main(String[] args) {	
		 launch(args);

	}
	@Override
	public void start(Stage window) throws IOException {
		
		AnchorPane overall=new AnchorPane();
		VBox MenuLayout = new VBox();
        Scene menu = new Scene(MenuLayout, 500, 400);
        Scene overallscene = new Scene(overall,500,425);
      
        // ---MenuBar
        MenuBar menuBar = new MenuBar();
        Menu menuProgram = new CustomMenuBar().createmenuprogram();        
        Menu menuAbout = new CustomMenuBar().createmenuabout();
        menuBar.getMenus().addAll(menuProgram, menuAbout);
        
        //---Histogram
        BarChart<String, Number> histogram = new CustomHistogramTab().create_Histogram();
        Group r_histogram = new Group(histogram);
        
        //---Logs
   	 	TableView<LogItem> lt=new CustomLogTab().createLog();
   	 	
        //---Table
   	 	TableView<CustomTableView> st=new CustomTableTab().createTab(studentsTab,lt);
   	 	HBox addremove = new CustomTableTab().CreateBottmpanel(st,lt);
   	 	
   	 	
   	 	
        //---Tabs
        TabPane tabpane = new CustomTabPane().create_tabpane(st, addremove, r_histogram,lt);
        
        //---Scene
        overall.getChildren().addAll(menuBar,tabpane);
        AnchorPane.setTopAnchor(menuBar, 0.0);
        AnchorPane.setTopAnchor(tabpane, 25.0);     
        window.setScene(overallscene);
        window.show();
	    	    }

	
	
	
	
}
