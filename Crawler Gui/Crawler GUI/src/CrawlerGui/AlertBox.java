package CrawlerGui;

import javafx.stage.*;
import javafx.geometry.Insets;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;


public class AlertBox extends AnchorPane {

	public static void display(String title, String message, String message2){
		Stage window = new Stage();
		
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle(title);
		
		Label label = new Label();
		label.setText(message);		
		
		Label label2 =new Label();
		label2.setText(message2);
		
		VBox layout = new VBox();
		
		VBox layout2 = new VBox();
		
		layout.getChildren().addAll(label);
		layout2.getChildren().addAll(label2);
		
		AnchorPane anchorpane = new AnchorPane();
	    Button closeButton = new Button("OK");
	    closeButton.minWidth(50);
	    closeButton.setOnAction(e -> window.close());
	    
	    HBox hbox = new HBox();
	    hbox.setPadding(new Insets(0, 10, 10, 10));
	    hbox.setSpacing(10);
	    hbox.getChildren().addAll(closeButton);

	    anchorpane.getChildren().addAll(layout,layout2,hbox);
	    AnchorPane.setBottomAnchor(hbox, 8.0);
	    AnchorPane.setRightAnchor(hbox, 5.0);
	    AnchorPane.setTopAnchor(layout, 30.0);
	    AnchorPane.setBottomAnchor(layout2, 30.0);
				
			
		
		Scene scene = new Scene(anchorpane,350,150);
		window.setScene(scene);
		window.showAndWait();
		
	}
}
