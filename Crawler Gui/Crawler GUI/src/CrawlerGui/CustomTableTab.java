package CrawlerGui;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

public class CustomTableTab extends AnchorPane{

TextField markInput,fNameInput,lNameInput,ageInput;

	public ObservableList<CustomTableView> getCustomTabViews(TableView<LogItem> lt) throws IOException{
		ObservableList<CustomTableView> students = FXCollections.observableArrayList();
		List<Student> studentsList=null;
		studentsList= StudentParser.parse(new File("students.txt"));
		for(Student el:studentsList){		
		students.add(new CustomTableView(el.getMark(), el.getFirstName(),el.getLastName(), el.getAge()));
		LogItem log = new LogItem();
		log.message=("ADDED: "+ el.getMark() + " " + el.getFirstName() + " " + el.getLastName() + " " + el.getAge() );
		lt.getItems().add(log);
		
		
		}
		return students;
		
	}
	public TableView<CustomTableView> createTab(TableView<CustomTableView> st,TableView<LogItem> lt) throws IOException{
		
		TableColumn<CustomTableView , Double> markColumn = new TableColumn<>("Mark");
        markColumn.setMinWidth(124);
        markColumn.setCellValueFactory(new PropertyValueFactory<>("mark"));
        
        TableColumn<CustomTableView , String> fNameColumn = new TableColumn<>("First Name");
        fNameColumn.setMinWidth(124);
        fNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        
        TableColumn<CustomTableView , String> lNameColumn = new TableColumn<>("Last Name");
        lNameColumn.setMinWidth(124);
        lNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        
        TableColumn<CustomTableView , Integer> ageColumn = new TableColumn<>("Age");
        ageColumn.setMinWidth(124);
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
        
        st=new TableView<>();
        st.setItems(new CustomTableTab().getCustomTabViews(lt));
        st.getColumns().addAll(markColumn,fNameColumn,lNameColumn,ageColumn);
				
		
		return st;
		}
	public HBox CreateBottmpanel(TableView<CustomTableView> st,TableView<LogItem> lt){
				
		markInput = new TextField();
    	markInput.setPromptText("Mark");
    	markInput.prefWidth(20);
    	
    	fNameInput = new TextField();
    	fNameInput.setPromptText("First Name");
    	fNameInput.prefWidth(20);
    	
    	lNameInput = new TextField();
    	lNameInput.setPromptText("Last Name");
    	lNameInput.prefWidth(20);
    	
    	ageInput = new TextField();
    	ageInput.setPromptText("Age");
    	ageInput.prefWidth(20);
    	
    	//---Buttons
    	Button addButton = new Button("ADD");
    	addButton.setOnAction(e -> {
			try {
				addButtonClicked(st,lt);
			} catch (IOException e1) {
				System.out.println("Error");
			}
		});
    	Button removeButton = new Button("REMOVE");         
    	removeButton.setOnAction(e -> removeButtonClicked(st,lt));
    	
    	
    
    HBox hbox = new HBox ();
    hbox.setPadding(new Insets(10,10,10,10));
    hbox.setSpacing(10);
    hbox.getChildren().addAll(markInput,fNameInput,lNameInput,ageInput,addButton,removeButton);
    
    return hbox;
	}

public void addButtonClicked(TableView<CustomTableView> st,TableView<LogItem> lt) throws IOException{
	try{
	CustomTableView student =new CustomTableView();
	student.setMark(Double.parseDouble(markInput.getText()));
	student.setFirstName(fNameInput.getText());
	student.setLastName(lNameInput.getText());
	student.setAge(Integer.parseInt(ageInput.getText()));
	st.getItems().add(student);
	LogItem log = new LogItem();
	log.message=("ADDED: "+ markInput.getText() + " " + fNameInput.getText() + " " + lNameInput.getText() + " " + ageInput.getText() );
	lt.getItems().add(log);
	}
	finally {
		
	
	markInput.clear();
	fNameInput.clear();
	lNameInput.clear();
	ageInput.clear();
			}
	}
public void removeButtonClicked(TableView<CustomTableView> st,TableView<LogItem> lt) {
	ObservableList<CustomTableView> studentSelected, allStudents;
	allStudents=st.getItems();
	studentSelected = st.getSelectionModel().getSelectedItems();
	LogItem log = new LogItem();
	log.message=("STUDENT REMOVED");
	lt.getItems().add(log);
	studentSelected.forEach(allStudents::remove);
	
	}
	



}

