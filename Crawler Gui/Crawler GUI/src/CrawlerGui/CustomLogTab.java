package CrawlerGui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

public class CustomLogTab extends AnchorPane{
	
	public TableView<LogItem> createLog(){
		
		
		TableColumn<LogItem , String> column = new TableColumn<>();
        column.setMinWidth(500);
        column.setCellValueFactory(new PropertyValueFactory<>("message"));
		
        TableView<LogItem> lt=new TableView<>();
        lt.setItems(getLogitem());
        lt.getColumns().add(column);
        
		return lt;
		}
	
	public ObservableList<LogItem> getLogitem(){
		ObservableList<LogItem> logs =FXCollections.observableArrayList();
		return logs;
	}
}

