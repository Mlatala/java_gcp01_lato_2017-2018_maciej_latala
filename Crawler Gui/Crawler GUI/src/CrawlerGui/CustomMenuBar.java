package CrawlerGui;


import java.awt.MenuBar;

import com.sun.xml.internal.ws.Closeable;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CustomMenuBar extends MenuBar {
	
	public MenuBar createmenubar(){
    MenuBar menuBar = new MenuBar();
	return menuBar;
	}
	
	public Menu createmenuprogram(){
    // --- Menu Program
    Menu menuProgram = new Menu("Program");
    	MenuItem close =new MenuItem("Close");
    	close.setAccelerator(KeyCombination.keyCombination("Ctrl+C"));
    	close.setOnAction(e ->System.exit(0));
    menuProgram.getItems().add(close);
    return menuProgram;
    }
	
	public Menu createmenuabout(){
    // --- Menu About
    Menu menuAbout = new Menu("About");
	menuAbout.setOnShown(e -> AlertBox.display("About","Program Informations","Author: Maciej Lata�a"));
	MenuItem add=new MenuItem();
    menuAbout.getItems().addAll(add);   
    return menuAbout;       
	}
}
