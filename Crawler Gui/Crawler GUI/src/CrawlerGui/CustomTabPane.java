package CrawlerGui;

import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class CustomTabPane extends AnchorPane{

	public TabPane create_tabpane(TableView<CustomTableView> st,HBox addremove,Group root,TableView<LogItem> lt){
		TabPane tabpane = new TabPane();
        
        Tab table_tab =new Tab("Students");
        VBox table_tab_vb =new VBox();
        table_tab_vb.getChildren().addAll(st,addremove);
        table_tab_vb.setPadding(new Insets(5));
        table_tab.setContent(table_tab_vb);
        
        Tab log_tab =new Tab("Log");
        HBox log_tab_hb = new HBox();
        log_tab_hb.getChildren().addAll(lt);
        log_tab_hb.setPadding(new Insets(5));
        log_tab.setContent(log_tab_hb);
        
        Tab histogram_tab =new Tab("Histogram");
        HBox histogram_tab_hb=new HBox();
        histogram_tab_hb.getChildren().addAll(root);
        histogram_tab_hb.setPadding(new Insets(5));
        histogram_tab.setContent(histogram_tab_hb);
        
        tabpane.getTabs().addAll(table_tab,log_tab,histogram_tab);
		
		
		return tabpane;

	}
}
