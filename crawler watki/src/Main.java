import java.util.ArrayList;
import java.util.List;

public class Main 
{

	public static void main( String[] args ) throws Exception
	{
		final Logger[] loggers = new Logger[]
		{
			new ConsoleLogger(),
			new MailLogger()
		};

		List<String> addressList =new ArrayList<>();
		addressList.add(Monitor.a0);addressList.add(Monitor.a1);addressList.add(Monitor.a2);addressList.add(Monitor.a3);addressList.add(Monitor.a4);addressList.add(Monitor.a5);addressList.add(Monitor.a6);addressList.add(Monitor.a7);addressList.add(Monitor.a8);addressList.add(Monitor.a9);
		Monitor m = new Monitor(addressList);

		m.addStudentaddedListener((Student)->loggers[0].log("ADDED",Student));
		m.addStudentaddedListener((Student)->loggers[1].log("ADDED",Student));
		m.addStudentremovedListener((Student)->loggers[0].log("REMOVED",Student));
		m.addStudentremovedListener((Student)->loggers[1].log("REMOVED",Student));
		
		try {
			m.start();
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("ERROR");
		}
	}
}