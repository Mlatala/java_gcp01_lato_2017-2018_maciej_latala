import java.util.ArrayList;
import java.util.List;

public class Monitor extends Thread{
	
	public List<String> addressList=null;
	public int numberOfThreads = 10;
	public boolean isWorking = true;
	
	Monitor(List<String> addressList)
	{		
		this.addressList=addressList;
	}
	
	
	public static String a0="students0.txt";
	public static String a1="students1.txt";
	public static String a2="students2.txt";
	public static String a3="students3.txt";
	public static String a4="students4.txt";
	public static String a5="students5.txt";
	public static String a6="students6.txt";
	public static String a7="students7.txt";
	public static String a8="students8.txt";
	public static String a9="students9.txt";
	
	
	private List<StudentListener> studentaddedListeners = new ArrayList<>();
    
    public void addStudentaddedListener(StudentListener listener){
        studentaddedListeners.add(listener);
    }
    public void removeStudentaddedListeners(StudentListener listener){
        studentaddedListeners.remove(listener);
    }

    
    
    private List<StudentListener> studentRemovedListener = new ArrayList<>();
    
    public void addStudentremovedListener(StudentListener listener){
        studentRemovedListener.add(listener);
    }
    public void removeStudentremovedListeners(StudentListener listener){
        studentaddedListeners.remove(listener);
    }
    
    public void cancel(){
        crawler.postCancel();  		
    }
    
    public void run(){
    	
    	crawler[] crawlerArray =new crawler[numberOfThreads];
    	try{
    	for(int i =0; i<numberOfThreads; i++)   			
    			crawlerArray[i]=new crawler(addressList.get(i));
    	}catch(Exception e){
    		e.printStackTrace();
    		System.out.println("Wiecej Watkow niz adresow");
    		cancel();
    	}
    	
		try {
			
			for(int i =0; i<numberOfThreads; i++)
	    		crawlerArray[i].start();

		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("ERROR");
		}
		
		try {
			Thread.sleep(25000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		cancel();
		
		for(int i =0; i<numberOfThreads; i++)
			while(crawlerArray[i].isAlive())
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		
		System.out.println("Zakonczono prace programu");	    
		
    } 
	
}
