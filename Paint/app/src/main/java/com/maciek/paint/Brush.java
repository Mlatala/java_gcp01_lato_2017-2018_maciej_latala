package com.maciek.paint;

import android.graphics.RectF;

public class Brush {

    public int color;
    public RectF shape;
    public String figure;

    public Brush(int color, RectF shape,String figure) {
        this.color = color;
        this.shape = shape;
        this.figure = figure;
    }

}