package com.maciek.paint;
import java.util.ArrayList;
import java.util.StringTokenizer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class PaintView extends SurfaceView implements SurfaceHolder.Callback {

    ArrayList<Brush> trace;
    Paint paint = new Paint();
    private int color;
    private int rozmiar;
    private String figure;

    public PaintView(Context context, AttributeSet attrs) {
        super(context, attrs);
        trace = new ArrayList<>();
        paint = new Paint();
        color = Color.RED;
        rozmiar = 20;
        figure = "oval";
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        // TODO Auto-generated method stub
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        // TODO Auto-generated method stub
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        // TODO Auto-generated method stub
    }

    public boolean onTouchEvent(MotionEvent event) {

        RectF marker = new RectF(event.getX() - rozmiar, event.getY() - rozmiar, event.getX() + rozmiar, event.getY() + rozmiar);
        trace.add(new Brush(color, marker,figure));
        invalidate();
        return true;
    }

    protected void onDraw(Canvas canvas) {

        for (Brush point : trace) {
            paint.setColor(point.color);
            Draw(canvas,point);
        }

    }

    public void Draw(Canvas canvas,Brush point){
        if (point.figure.equals("oval"))
            canvas.drawOval(point.shape,paint);
        else if (point.figure.equals("square"))
            canvas.drawRect(point.shape,paint);
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setFigure(String figure){this.figure=figure;}

    public int getRozmiar() { return rozmiar;}

    public void setRozmiar(int rozmiar) {
        this.rozmiar = rozmiar;
    }
}