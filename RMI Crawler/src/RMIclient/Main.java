package RMIclient;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import javax.naming.NamingException;

import Features.CrawlerServer;
import Features.MessageEvent;

public class Main {
	public static void main( String[] args ) throws NamingException, RemoteException, NotBoundException
	{

		String host = "localhost";
		int port = 5070;
		
		String name = "rmi://" + port + "/RMIcrawlerproxy";
		
		Registry registry = LocateRegistry.getRegistry( host, port ); 
		CrawlerServer cs = (CrawlerServer) registry.lookup( name );
		
				
		cs.setMessageEvent( new CustomMessageEvent() );
		cs.sendMessages();
		cs.run();
	}
	private static class CustomMessageEvent extends UnicastRemoteObject implements MessageEvent
	{
		private static final long serialVersionUID = 6738492455439057283L;
		
		protected CustomMessageEvent() throws RemoteException
		{
			super();
		}
		
		@Override
		public void messageSended( String message )
		{
			System.out.printf( "Server message: %s\n", message );
		}
	}
}
