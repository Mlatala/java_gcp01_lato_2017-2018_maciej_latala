package Crawler;

@FunctionalInterface
public interface StudentListener {
	void handle(Student student);
}
