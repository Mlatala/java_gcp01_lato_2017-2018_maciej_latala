package Crawler;

import java.util.ArrayList;
import java.util.List;

public class Main 
{

	public static void main( String[] args ) throws Exception
	{
		final Logger[] loggers = new Logger[]
		{
			new ConsoleLogger(),
		};
		
		String a0="students0.txt";
		
		crawler c = new crawler(a0);
		
		c.addStudentaddedListener((Student)->loggers[0].log("ADDED",Student));
		c.addStudentremovedListener((Student)->loggers[0].log("REMOVED",Student));
		
		try {
			c.run();
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("ERROR");
		}
	}
}