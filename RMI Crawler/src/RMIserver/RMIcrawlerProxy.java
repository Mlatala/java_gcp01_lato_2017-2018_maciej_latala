package RMIserver;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Crawler.ConsoleLogger;
import Crawler.ExtremumMode;
import Crawler.IterationListener;
import Crawler.Logger;
import Crawler.OrderMode;
import Crawler.Student;
import Crawler.StudentListener;
import Crawler.StudentsParser;
import Crawler.crawlerException;
import Features.CrawlerServer;
import Features.MessageEvent;

public class RMIcrawlerProxy extends UnicastRemoteObject implements CrawlerServer
{
	private static final long serialVersionUID = -2101656626166332642L;
	

	public static boolean isWorking = true;
	public String address=null;
    private List<Student> studentsList=null;
    private List<Student> secondaryStudentsList=null;
    public static boolean flag = false;
    private MessageEvent event = null;
    
    public RMIcrawlerProxy(String address) throws RemoteException
    {
        this.address=address;
    }

	
	@Override
	public void run() throws RemoteException
	{
		  int iteration = 1;
	        while(isWorking){
	        	System.out.println("Rozpoczeto iteracje "+iteration);
	            if(address == null)
					try {
						throw new crawlerException("Zly Adres");
					} catch (crawlerException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

	            for(IterationListener el:iterationStartedListeners){
	                el.handle(iteration);
	               
	            }
	            secondaryStudentsList=studentsList;
	            try {
					studentsList= StudentsParser.parse(new File(address));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

	            if(secondaryStudentsList==null)
	            {
	                for (Student el:studentsList) {
	                    for (StudentListener e : studentaddedListeners)
	                        e.handle(el);
	                }
	            }

	            if(studentsList!=null && secondaryStudentsList!=null) {


	                for (Student el : studentsList) {
	                    for (Student el2 : secondaryStudentsList) {
	                        if (el.equals(el2)) {
	                            flag = false;
	                            break;
	                        } else {
	                            flag = true;

	                        }
	                    }
	                    if (flag) {
	                        for (StudentListener e : studentaddedListeners) {

	                        	e.handle(el);
	                        }
	                    }
	                }

	                boolean flag2 = false;
	                for (Student el : secondaryStudentsList) {
	                    for (Student el2 : studentsList) {
	                        if (el.equals(el2)) {
	                            flag2 = false;
	                            break;
	                        } else {     
	                            flag2 = true;
	                        }
	                    }
	                    if (flag2) {
	                        for (StudentListener e : studentRemovedListener) {
	                    		e.handle(el);
	                        }
	                    }
	                }
	            }

	            
	            System.out.println("Age: <"+ extractAge(ExtremumMode.MIN,studentsList)+","+extractAge(ExtremumMode.MAX,studentsList)+">");
	            System.out.println("Mark: <"+ extractMark(ExtremumMode.MIN,studentsList)+","+extractMark(ExtremumMode.MAX,studentsList)+">");
	            System.out.println("Ordered by MARK: ");
	            studentsList = extractStudents(OrderMode.MARK,studentsList);         
	            
	            for (Student el:studentsList )
	            {
	                System.out.println(el.getMark() + " "+ el.getFirstName()+ " "+ el.getLastName()+ " "+ el.getAge());
	            }
	            System.out.println("");

	            try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	            for(IterationListener el:iterationComplitedListeners){
	                el.handle(iteration);
	            }
	            System.out.println("Zakonczono iteracje "+iteration+"\n");
	            iteration++;

	        }


	    }

	
private List<IterationListener> iterationStartedListeners = new ArrayList<>();
    @Override
    public void addIterationStartedListener(IterationListener listener){
        iterationStartedListeners.add(listener);
    }
    @Override
    public void removeIterationStartedListener(IterationListener listener){iterationStartedListeners.remove(listener); }
    
    

    private List<IterationListener> iterationComplitedListeners = new ArrayList<>();
    @Override
    public void addIterationComplitedListener(IterationListener listener){
        iterationComplitedListeners.add(listener);
    }      
    @Override
    public void removeIterationComplitedListener(IterationListener listener){ iterationComplitedListeners.remove(listener); }
    
    

    private List<StudentListener> studentaddedListeners = new ArrayList<>();
    @Override 
    public void addStudentaddedListener(StudentListener listener){
        studentaddedListeners.add(listener);
    }
    @Override
    public void removeStudentaddedListeners(StudentListener listener){
        studentaddedListeners.remove(listener);
    }

    
    
    private List<StudentListener> studentRemovedListener = new ArrayList<>();
    @Override
    public void addStudentremovedListener(StudentListener listener){
        studentRemovedListener.add(listener);
    }
    @Override
    public void removeStudentremovedListeners(StudentListener listener){
        studentaddedListeners.remove(listener);
    }

    @Override
    public List<Student> extractStudents(OrderMode mode,List<Student> students)
    {
        switch(mode) {
            case AGE:{Collections.sort(students,Student.Comparators.AGE);}break;
            case FIRST_NAME:{Collections.sort(students,Student.Comparators.FIRST_NAME);}break;
            case LAST_NAME:{Collections.sort(students,Student.Comparators.LAST_NAME);}break;
            case MARK:{Collections.sort(students,Student.Comparators.MARK);}break;
            default:{}break;

        }
        return students;
    }
    @Override
    public double extractMark(ExtremumMode mode,List<Student> students)
    {
        Student result=new Student();
        switch(mode){
            case MAX:{result=Collections.max(students,Student.Comparators.MARK);}break;
            case MIN:{result=Collections.min(students,Student.Comparators.MARK);}break;
            default:{}break;
        }
        return result.getMark();
    }
    @Override
    public int extractAge (ExtremumMode mode,List<Student> students) 
    {
        Student result=new Student();
        switch(mode){
            case MAX:{result=Collections.max(students,Student.Comparators.AGE);}break;
            case MIN:{result=Collections.min(students,Student.Comparators.AGE);}break;
            default:{}break;
        }
        return result.getAge();
    }

    final Logger[] loggers = new Logger[]
    		{
    			new ConsoleLogger(),
    		};
    @Override
	public void setMessageEvent( MessageEvent event ) throws RemoteException
	{		
		this.event = event;		
	}
	
    @Override
	public void sendMessages() throws RemoteException
	{
		
		if(this.event == null)
			return;
		
		this.event.messageSended( "Callback" );

	}
}