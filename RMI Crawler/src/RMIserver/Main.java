package RMIserver;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Scanner;

import javax.naming.NamingException;

import Crawler.ConsoleLogger;

import Crawler.Logger;

import Crawler.crawler;


public class Main{
	public static void main( String[] args ) throws NamingException, RemoteException, InterruptedException, AlreadyBoundException
	{
		int port = 5070;

		
		String name = "rmi://" + port + "/RMIcrawlerproxy";
		Registry registry = LocateRegistry.createRegistry( port ); 
		
		final Logger[] loggers = new Logger[]
				{
					new ConsoleLogger(),
				};
				
				String a0="students0.txt";
		
		try
		{	

			RMIcrawlerProxy c = new RMIcrawlerProxy(a0); 		
			c.addStudentaddedListener((Student)->loggers[0].log("ADDED",Student));
			c.addStudentremovedListener((Student)->loggers[0].log("REMOVED",Student));
			registry.bind( name, c ); 

			System.out.println( "Type 'exit' to exit server." );
			Scanner scanner = new Scanner( System.in );

			while ( true )
			{
									
				if ( scanner.hasNextLine() )
				{	
					if ( "exit".equals( scanner.nextLine() ) )
						break;
				}
			}
			
			scanner.close();
		}
		finally
		{
			UnicastRemoteObject.unexportObject( registry, true ); 
			System.out.println( "Server stopped." ); 
		}
	}
	
	
	
}
