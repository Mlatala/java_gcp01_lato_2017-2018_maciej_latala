package Features;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import Crawler.ExtremumMode;
import Crawler.IterationListener;
import Crawler.OrderMode;
import Crawler.Student;
import Crawler.StudentListener;

public interface CrawlerServer extends Remote{

    public void addIterationStartedListener(IterationListener listener)throws RemoteException;
    public void removeIterationStartedListener(IterationListener listener)throws RemoteException;
    
    public void addIterationComplitedListener(IterationListener listener)throws RemoteException;
    public void removeIterationComplitedListener(IterationListener listener)throws RemoteException;
    
    public void addStudentaddedListener(StudentListener listener)throws RemoteException;
    public void removeStudentaddedListeners(StudentListener listener)throws RemoteException;
    
    public void addStudentremovedListener(StudentListener listener)throws RemoteException;
    public void removeStudentremovedListeners(StudentListener listener)throws RemoteException;
    
    public List<Student> extractStudents(OrderMode mode,List<Student> students)throws RemoteException;
    
    public double extractMark(ExtremumMode mode,List<Student> students)throws RemoteException;
    
    public int extractAge(ExtremumMode mode,List<Student> students)throws RemoteException;

    public void run() throws RemoteException ;	
	
	public void sendMessages() throws RemoteException;
	public void setMessageEvent(MessageEvent event) throws RemoteException;

}
