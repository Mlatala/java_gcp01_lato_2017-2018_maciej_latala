package application;

import java.util.List;
import java.io.File;
import java.io.IOException;
import java.net.URL;
	import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
	import javafx.fxml.Initializable;
	import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


	public class Tab0ControlController implements Initializable{
		public ObservableList<Student> getCustomTabViews() throws IOException{
			ObservableList<Student> students = FXCollections.observableArrayList();
			List<Student> studentsList=null;
			studentsList= StudentParser.parse(new File("students.txt"));
			for(Student el:studentsList){		
			students.add(new Student(el.getMark(), el.getFirstName(),el.getLastName(), el.getAge()));
			}	
			return students;		
			}
		
		@FXML
		TableColumn<Student , Double> markColumn;
		@FXML
		TableColumn<Student , String> fnameColumn;
		@FXML
		TableColumn<Student , String> lnameColumn;
		@FXML
		TableColumn <Student , Integer>ageColumn;

		@FXML
		TableView<Student> studentTable;
		

		

			@FXML
			public void initialize() throws IOException{
	    	
	    	markColumn.setCellValueFactory(new PropertyValueFactory<>("mark"));
	    	
	    	fnameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
	    	
	    	lnameColumn.setCellValueFactory(new PropertyValueFactory<>("secondName"));

	    	ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
	    	
	    	studentTable.setItems(getCustomTabViews());
	    	studentTable.getColumns().addAll(markColumn,fnameColumn,lnameColumn,ageColumn);
			}
		
		@Override
		public void initialize(URL location, ResourceBundle resources) {
			// TODO Auto-generated method stub
			;
		}
	}

