package application;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class MainController {

	@FXML 
	private Tab1ControlController tab1ControlController;
	
	@FXML
	public void handleclose(){
		Platform.exit();
	}
	
}
