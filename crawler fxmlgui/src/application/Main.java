package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class Main extends Application {

	Stage window;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Login_screenConroller lsc=new Login_screenConroller();
			
			window=primaryStage;
			AnchorPane root= FXMLLoader.load(getClass().getResource("Login_screen.fxml"));
			Scene scene = new Scene(root,400,200);
			window.setScene(scene);
			
			window.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}
