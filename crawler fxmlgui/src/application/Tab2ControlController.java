package application;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

public class Tab2ControlController implements Initializable{

	@FXML
	private BarChart<String, Number> barChart;
	@FXML
	private CategoryAxis xAxis;
	@FXML
	private NumberAxis yAxis;
    
	public int count_2=0,count_2_5=0,count_3=0,count_3_5=0,count_4=0,count_4_5=0,count_5=0;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		xAxis.setCategories(FXCollections.<String>
		observableList(Arrays.asList("2.0", "2.5" , "3.0" , "3.5" , "4.0" , "4.5" , "5.0" )));
		xAxis.setLabel("Marks");
		yAxis.setLabel("Count");

		
		try {
			marks_count(count_2, count_2_5, count_3, count_3_5, count_4, count_4_5, count_5);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Series<String, Number> series1 = new XYChart.Series<>();
	    series1.setName("Marks");
	    
	    series1.getData().add(new XYChart.Data<>("2.0",count_2));
	    series1.getData().add(new XYChart.Data<>("2.5", count_2_5));
	    series1.getData().add(new XYChart.Data<>("3.0", count_3));
	    series1.getData().add(new XYChart.Data<>("3.5", count_3_5));
	    series1.getData().add(new XYChart.Data<>("4.0", count_4));
	    series1.getData().add(new XYChart.Data<>("4.5",count_4_5));
	    series1.getData().add(new XYChart.Data<>("5.0", count_5));

	    
	    barChart.getData().add(series1);
	}

	public void marks_count(int count_2,int count_2_5,int count_3,int count_3_5,int count_4,int count_4_5,int count_5) throws IOException{
		List<Student> studentsList=null;
		studentsList= StudentParser.parse(new File("students.txt"));
		
		for(Student el:studentsList){		
			if(el.getMark()==2.0)
			count_2++;			
			else if(el.getMark()==2.5)
				count_2_5++;
			else if(el.getMark()==3.0)
				count_3++;
			else if(el.getMark()==3.5)
				count_3_5++;
			else if(el.getMark()==4.0)
				count_4++;
			else if(el.getMark()==4.5)
				count_4_5++;
			else if(el.getMark()==5.0)
				count_5++;
			
		}
		this.count_2=count_2;
		this.count_2_5=count_2_5;
		this.count_3=count_3;
		this.count_3_5=count_3_5;
		this.count_4=count_4;
		this.count_4_5=count_4_5;
		this.count_5=count_5;
	}
}
